Rails.application.routes.draw do
  get 'newsletters/create'
  namespace(:admin){
    resources :assistances
    resources :newsletters 
    resources(:notices){
      resources :notice_images do
        put :sort, on: :collection
        get :unpublished, on: :member 
        get :published, on: :member 
      end
    }
    resources :abouts, :only => [:edit, :update] 
    resources :configurations, :only => [:edit, :update] 
    get 'homes/index'
    resources(:product_labels) do
      collection do
        get :check_images
      end  
    end
    resources(:product_lines ){
      resources :product_categories
      resources(:products){
        resources :product_images do
          put :sort, on: :collection
          get :unpublished, on: :member 
          get :published, on: :member
        end
        collection do
          get :check_images
          get :check_product_download
        end
      } 
    }
    resources :users
    root :to => 'homes#index'
  }

    
  localized do
    
    namespace(:furnitures){
      resources :categories, :only => [:show] 
      resources :products, :only => [:show] 
      root :to => 'homes#index'
      match '/search' => 'homes#search', via: :get
    }
    namespace(:toys){
      resources :categories, :only => [:show] 
      resources :products, :only => [:show]  
      resources(:homes, :only => [:index]){
        
      }   
      root :to => 'homes#index'
      match '/search' => 'homes#search', via: :get
      
    }
    namespace(:domestic_utilities){
      resources :categories, :only => [:show] 
      resources :products, :only => [:show]
      root :to => 'homes#index'
      match '/search' => 'homes#search', via: :get
    }
    resources :newsletters, :only => [:create]
    resources :news, :only => [:index, :show] 
    resources :user_sessions, :only => [:new, :create, :destroy] 
    root :to => 'homes#index'
    get 'homes/load_image'
    match '/login' => 'user_sessions#new', via: :get  
    match '/logout' => 'user_sessions#destroy', via: :get
    match '/about' => 'homes#about', via: :get
    match '/assistance' =>  'homes#assistance', via: :get

  end

end