class ActiveRecord::Base
  #Metodo que substitui o destroy
  def newdestroy
    self.active = false
    self.save
  end
    
  # Metodo para listar somente os ativos (Não "Deletados")
  def self.all_active(ordernar ='')
    order_show = 'id'
    if !ordernar.blank? 
      order_show = ordernar
    end
    where(active: true).order(order_show)
  end
  
  # Metodo para listar todos publicados
  def self.all_published(ordernar ='')
    order_show = 'id'
    if !ordernar.blank? 
      order_show = ordernar
    end
    where(active: true, published: true).order(order_show)
  end  
  
  def self.find_published(id)
    find_by_id(id, :conditions => ['published =  true and active = true'])
  end
  
end