# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( bootstrap.min.css )
Rails.application.config.assets.precompile += %w( bootstrap.min.js )


Rails.application.config.assets.precompile += %w( admin.css )
Rails.application.config.assets.precompile += %w( admin.js )

Rails.application.config.assets.precompile += %w( luminous-basic.min.css )
Rails.application.config.assets.precompile += %w( Luminous.min.js )
Rails.application.config.assets.precompile += %w( assistance.js )

Rails.application.config.assets.precompile += %w( furnitures.css )
Rails.application.config.assets.precompile += %w( furnitures.js )

Rails.application.config.assets.precompile += %w( toys.css )
Rails.application.config.assets.precompile += %w( toys.js )

Rails.application.config.assets.precompile += %w( uds.css )
Rails.application.config.assets.precompile += %w( uds.js )
