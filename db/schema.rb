# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170613145042) do

  create_table "about_translations", force: :cascade do |t|
    t.integer  "about_id",               limit: 4,     null: false
    t.string   "locale",                 limit: 255,   null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "title_about",            limit: 255
    t.text     "description_about",      limit: 65535
    t.string   "title_newsletter",       limit: 255
    t.text     "description_newsletter", limit: 65535
    t.string   "title_movie",            limit: 255
    t.text     "description_movie",      limit: 65535
    t.string   "title_injectors",        limit: 255
    t.string   "subtitle_injectors",     limit: 255
    t.string   "title_tooling",          limit: 255
    t.string   "subtitle_tooling",       limit: 255
    t.string   "title_metallization",    limit: 255
    t.string   "subtitle_metallization", limit: 255
  end

  add_index "about_translations", ["about_id"], name: "index_about_translations_on_about_id", using: :btree
  add_index "about_translations", ["locale"], name: "index_about_translations_on_locale", using: :btree

  create_table "abouts", force: :cascade do |t|
    t.string   "title_about",                      limit: 255
    t.text     "description_about",                limit: 65535
    t.string   "title_newsletter",                 limit: 255
    t.text     "description_newsletter",           limit: 65535
    t.string   "movie_link",                       limit: 255
    t.string   "title_movie",                      limit: 255
    t.text     "description_movie",                limit: 65535
    t.boolean  "active",                                         default: true, null: false
    t.boolean  "published",                                      default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_small_file_name",            limit: 255
    t.string   "image_small_content_type",         limit: 255
    t.integer  "image_small_file_size",            limit: 4
    t.datetime "image_small_updated_at"
    t.string   "image_large_file_name",            limit: 255
    t.string   "image_large_content_type",         limit: 255
    t.integer  "image_large_file_size",            limit: 4
    t.datetime "image_large_updated_at"
    t.string   "image_icon_file_name",             limit: 255
    t.string   "image_icon_content_type",          limit: 255
    t.integer  "image_icon_file_size",             limit: 4
    t.datetime "image_icon_updated_at"
    t.string   "numbers_injectors",                limit: 255
    t.string   "title_injectors",                  limit: 255
    t.string   "subtitle_injectors",               limit: 255
    t.string   "title_tooling",                    limit: 255
    t.string   "subtitle_tooling",                 limit: 255
    t.string   "title_metallization",              limit: 255
    t.string   "subtitle_metallization",           limit: 255
    t.string   "image_injectors_file_name",        limit: 255
    t.string   "image_injectors_content_type",     limit: 255
    t.integer  "image_injectors_file_size",        limit: 4
    t.datetime "image_injectors_updated_at"
    t.string   "image_tooling_file_name",          limit: 255
    t.string   "image_tooling_content_type",       limit: 255
    t.integer  "image_tooling_file_size",          limit: 4
    t.datetime "image_tooling_updated_at"
    t.string   "image_metallization_file_name",    limit: 255
    t.string   "image_metallization_content_type", limit: 255
    t.integer  "image_metallization_file_size",    limit: 4
    t.datetime "image_metallization_updated_at"
    t.string   "number_injectors",                 limit: 255,   default: ""
  end

  create_table "assistances", force: :cascade do |t|
    t.string   "state",       limit: 255
    t.string   "name",        limit: 255
    t.string   "phone",       limit: 255
    t.string   "address",     limit: 255
    t.string   "district",    limit: 255
    t.string   "city",        limit: 255
    t.string   "zip_code",    limit: 255
    t.string   "email",       limit: 255
    t.string   "site",        limit: 255
    t.text     "observation", limit: 65535
    t.boolean  "active",                    default: true, null: false
    t.boolean  "published",                 default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "configuration_translations", force: :cascade do |t|
    t.integer  "configuration_id",                         limit: 4,     null: false
    t.string   "locale",                                   limit: 255,   null: false
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "title_telesales",                          limit: 255
    t.string   "title_contact",                            limit: 255
    t.string   "title_institutional",                      limit: 255
    t.string   "title_virtual_catalog",                    limit: 255
    t.string   "title_download_virtual_catalog",           limit: 255
    t.string   "text_download_virtual_catalog",            limit: 255
    t.string   "title_download",                           limit: 255
    t.string   "title_search",                             limit: 255
    t.string   "title_more",                               limit: 255
    t.string   "title_product_data",                       limit: 255
    t.string   "title_length",                             limit: 255
    t.string   "title_width",                              limit: 255
    t.string   "title_height",                             limit: 255
    t.string   "title_weight",                             limit: 255
    t.string   "title_colors",                             limit: 255
    t.string   "title_description",                        limit: 255
    t.string   "title_product_specifications",             limit: 255
    t.string   "title_quantity",                           limit: 255
    t.string   "title_see_the_product",                    limit: 255
    t.string   "title_production_xplast",                  limit: 255
    t.string   "title_contact_us",                         limit: 255
    t.string   "phone_number",                             limit: 255
    t.string   "sac_number",                               limit: 255
    t.text     "description_about",                        limit: 65535
    t.text     "description_distributed",                  limit: 65535
    t.string   "title_assistance",                         limit: 255
    t.string   "title_enter",                              limit: 255
    t.string   "title_packed_product_specifications_pack", limit: 255
    t.string   "title_parts",                              limit: 255
    t.string   "title_nothing_found",                      limit: 255
    t.string   "title_phone",                              limit: 255
    t.string   "title_address",                            limit: 255
    t.string   "title_neighborhood",                       limit: 255
    t.string   "title_city",                               limit: 255
    t.string   "title_email",                              limit: 255
    t.string   "title_site",                               limit: 255
    t.string   "title_service_text",                       limit: 255
    t.string   "title_currency_exchange",                  limit: 255
    t.string   "title_know_more",                          limit: 255
    t.string   "title_news",                               limit: 255
    t.string   "title_events",                             limit: 255
    t.string   "title_download_images",                    limit: 255
    t.string   "title_source_news",                        limit: 255
    t.string   "title_send_button",                        limit: 255
    t.string   "title_share",                              limit: 255
    t.string   "newsletter_success",                       limit: 255
    t.string   "newsletter_error",                         limit: 255
  end

  add_index "configuration_translations", ["configuration_id"], name: "index_configuration_translations_on_configuration_id", using: :btree
  add_index "configuration_translations", ["locale"], name: "index_configuration_translations_on_locale", using: :btree

  create_table "configurations", force: :cascade do |t|
    t.string   "title_telesales",                          limit: 255
    t.string   "title_contact",                            limit: 255
    t.string   "title_institutional",                      limit: 255
    t.string   "title_virtual_catalog",                    limit: 255
    t.string   "title_download_virtual_catalog",           limit: 255
    t.string   "text_download_virtual_catalog",            limit: 255
    t.string   "title_download",                           limit: 255
    t.string   "title_search",                             limit: 255
    t.string   "title_more",                               limit: 255
    t.string   "title_product_data",                       limit: 255
    t.string   "title_length",                             limit: 255
    t.string   "title_width",                              limit: 255
    t.string   "title_height",                             limit: 255
    t.string   "title_weight",                             limit: 255
    t.string   "title_colors",                             limit: 255
    t.string   "title_description",                        limit: 255
    t.string   "title_product_specifications",             limit: 255
    t.string   "title_quantity",                           limit: 255
    t.string   "title_see_the_product",                    limit: 255
    t.string   "title_production_xplast",                  limit: 255
    t.string   "title_contact_us",                         limit: 255
    t.string   "phone_number",                             limit: 255
    t.string   "sac_number",                               limit: 255
    t.text     "description_about",                        limit: 65535
    t.text     "description_distributed",                  limit: 65535
    t.string   "title_assistance",                         limit: 255
    t.string   "sac_mail",                                 limit: 255
    t.string   "link_facebook",                            limit: 255
    t.string   "link_instagram",                           limit: 255
    t.string   "link_linkedin",                            limit: 255
    t.boolean  "active",                                                 default: true, null: false
    t.boolean  "published",                                              default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title_enter",                              limit: 255
    t.string   "catalog_furniture_file_name",              limit: 255
    t.string   "catalog_furniture_content_type",           limit: 255
    t.integer  "catalog_furniture_file_size",              limit: 4
    t.datetime "catalog_furniture_updated_at"
    t.string   "catalog_toys_file_name",                   limit: 255
    t.string   "catalog_toys_content_type",                limit: 255
    t.integer  "catalog_toys_file_size",                   limit: 4
    t.datetime "catalog_toys_updated_at"
    t.string   "catalog_domestic_utility_file_name",       limit: 255
    t.string   "catalog_domestic_utility_content_type",    limit: 255
    t.integer  "catalog_domestic_utility_file_size",       limit: 4
    t.datetime "catalog_domestic_utility_updated_at"
    t.string   "title_packed_product_specifications_pack", limit: 255
    t.string   "title_parts",                              limit: 255
    t.string   "title_nothing_found",                      limit: 255
    t.string   "title_phone",                              limit: 255
    t.string   "title_address",                            limit: 255
    t.string   "title_neighborhood",                       limit: 255
    t.string   "title_city",                               limit: 255
    t.string   "title_email",                              limit: 255
    t.string   "title_site",                               limit: 255
    t.string   "title_service_text",                       limit: 255
    t.string   "title_currency_exchange",                  limit: 255
    t.string   "title_know_more",                          limit: 255
    t.string   "title_news",                               limit: 255
    t.string   "title_events",                             limit: 255
    t.string   "title_download_images",                    limit: 255
    t.string   "title_source_news",                        limit: 255
    t.string   "title_send_button",                        limit: 255
    t.string   "title_share",                              limit: 255
    t.string   "newsletter_success",                       limit: 255
    t.string   "newsletter_error",                         limit: 255
  end

  create_table "newsletters", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.string   "email",      limit: 255
    t.boolean  "active",                 default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notice_image_translations", force: :cascade do |t|
    t.integer  "notice_image_id", limit: 4,   null: false
    t.string   "locale",          limit: 255, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "title",           limit: 255
  end

  add_index "notice_image_translations", ["locale"], name: "index_notice_image_translations_on_locale", using: :btree
  add_index "notice_image_translations", ["notice_image_id"], name: "index_notice_image_translations_on_notice_image_id", using: :btree

  create_table "notice_images", force: :cascade do |t|
    t.integer  "notice_id",                 limit: 4
    t.string   "title",                     limit: 255
    t.boolean  "active",                                default: true, null: false
    t.boolean  "published",                             default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_notice_file_name",    limit: 255
    t.string   "image_notice_content_type", limit: 255
    t.integer  "image_notice_file_size",    limit: 4
    t.datetime "image_notice_updated_at"
    t.integer  "priority",                  limit: 4,   default: 1,    null: false
  end

  add_index "notice_images", ["notice_id"], name: "index_notice_images_on_notice_id", using: :btree

  create_table "notice_translations", force: :cascade do |t|
    t.integer  "notice_id",   limit: 4,     null: false
    t.string   "locale",      limit: 255,   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "title",       limit: 255
    t.text     "description", limit: 65535
  end

  add_index "notice_translations", ["locale"], name: "index_notice_translations_on_locale", using: :btree
  add_index "notice_translations", ["notice_id"], name: "index_notice_translations_on_notice_id", using: :btree

  create_table "notices", force: :cascade do |t|
    t.string   "title",                     limit: 255
    t.text     "description",               limit: 65535
    t.string   "title_source",              limit: 255
    t.string   "slug",                      limit: 255
    t.boolean  "active",                                  default: true,  null: false
    t.boolean  "published",                               default: true,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "notice_image_file_name",    limit: 255
    t.string   "notice_image_content_type", limit: 255
    t.integer  "notice_image_file_size",    limit: 4
    t.datetime "notice_image_updated_at"
    t.boolean  "its_an_event",                            default: false, null: false
    t.date     "start_event"
    t.date     "stop_event"
    t.string   "link_source",               limit: 255
  end

  add_index "notices", ["slug"], name: "index_notices_on_slug", unique: true, using: :btree

  create_table "product_categories", force: :cascade do |t|
    t.integer  "product_line_id",             limit: 4
    t.string   "title",                       limit: 255
    t.boolean  "active",                                  default: true, null: false
    t.boolean  "published",                               default: true, null: false
    t.string   "slug",                        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_category_file_name",    limit: 255
    t.string   "image_category_content_type", limit: 255
    t.integer  "image_category_file_size",    limit: 4
    t.datetime "image_category_updated_at"
    t.boolean  "featured"
    t.string   "class_toys",                  limit: 255
    t.string   "icon_category_file_name",     limit: 255
    t.string   "icon_category_content_type",  limit: 255
    t.integer  "icon_category_file_size",     limit: 4
    t.datetime "icon_category_updated_at"
    t.integer  "priority",                    limit: 4,   default: 1,    null: false
  end

  add_index "product_categories", ["slug"], name: "index_product_categories_on_slug", unique: true, using: :btree

  create_table "product_category_translations", force: :cascade do |t|
    t.integer  "product_category_id", limit: 4,   null: false
    t.string   "locale",              limit: 255, null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "title",               limit: 255
    t.string   "slug",                limit: 255
  end

  add_index "product_category_translations", ["locale"], name: "index_product_category_translations_on_locale", using: :btree
  add_index "product_category_translations", ["product_category_id"], name: "index_product_category_translations_on_product_category_id", using: :btree

  create_table "product_images", force: :cascade do |t|
    t.integer  "product_id",                      limit: 4
    t.boolean  "active",                                      default: true, null: false
    t.boolean  "published",                                   default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "product_image_file_file_name",    limit: 255
    t.string   "product_image_file_content_type", limit: 255
    t.integer  "product_image_file_file_size",    limit: 4
    t.datetime "product_image_file_updated_at"
    t.integer  "priority",                        limit: 4,   default: 1,    null: false
  end

  add_index "product_images", ["product_id"], name: "index_product_images_on_product_id", using: :btree

  create_table "product_labels", force: :cascade do |t|
    t.string   "title",                    limit: 255
    t.boolean  "active",                               default: true, null: false
    t.boolean  "published",                            default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_label_file_name",    limit: 255
    t.string   "image_label_content_type", limit: 255
    t.integer  "image_label_file_size",    limit: 4
    t.datetime "image_label_updated_at"
  end

  create_table "product_line_translations", force: :cascade do |t|
    t.integer  "product_line_id",   limit: 4,     null: false
    t.string   "locale",            limit: 255,   null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "title",             limit: 255
    t.string   "short_description", limit: 255
    t.text     "description",       limit: 65535
  end

  add_index "product_line_translations", ["locale"], name: "index_product_line_translations_on_locale", using: :btree
  add_index "product_line_translations", ["product_line_id"], name: "index_product_line_translations_on_product_line_id", using: :btree

  create_table "product_lines", force: :cascade do |t|
    t.string   "title",                           limit: 255
    t.string   "short_description",               limit: 255
    t.text     "description",                     limit: 65535
    t.string   "slug",                            limit: 255,                  null: false
    t.boolean  "published",                                     default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "product_line_image_file_name",    limit: 255
    t.string   "product_line_image_content_type", limit: 255
    t.integer  "product_line_image_file_size",    limit: 4
    t.datetime "product_line_image_updated_at"
  end

  add_index "product_lines", ["slug"], name: "index_product_lines_on_slug", unique: true, using: :btree

  create_table "product_translations", force: :cascade do |t|
    t.integer  "product_id",     limit: 4,   null: false
    t.string   "locale",         limit: 255, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "title",          limit: 255
    t.string   "description",    limit: 255
    t.string   "product_colors", limit: 255
  end

  add_index "product_translations", ["locale"], name: "index_product_translations_on_locale", using: :btree
  add_index "product_translations", ["product_id"], name: "index_product_translations_on_product_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.integer  "product_line_id",               limit: 4
    t.integer  "product_category_id",           limit: 4
    t.string   "reference",                     limit: 255
    t.string   "ean",                           limit: 255
    t.string   "product_length",                limit: 255
    t.string   "product_width",                 limit: 255
    t.string   "product_height",                limit: 255
    t.string   "product_weight",                limit: 255
    t.string   "product_parts",                 limit: 255
    t.string   "product_minimum_age",           limit: 255
    t.string   "product_packed_length",         limit: 255
    t.string   "product_packed_width",          limit: 255
    t.string   "product_packed_height",         limit: 255
    t.string   "product_packed_weight",         limit: 255
    t.string   "product_packed_quantity",       limit: 255
    t.boolean  "new",                                       default: false, null: false
    t.boolean  "featured",                                  default: false, null: false
    t.boolean  "active",                                    default: true,  null: false
    t.boolean  "published",                                 default: true,  null: false
    t.string   "slug",                          limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_prod_file_name",          limit: 255
    t.string   "image_prod_content_type",       limit: 255
    t.integer  "image_prod_file_size",          limit: 4
    t.datetime "image_prod_updated_at"
    t.integer  "product_label_id",              limit: 4
    t.string   "movie_link",                    limit: 255
    t.string   "cubage",                        limit: 255
    t.string   "unit",                          limit: 255
    t.string   "packing",                       limit: 255
    t.string   "dun",                           limit: 255
    t.string   "cnm",                           limit: 255
    t.string   "plastic_bag_length",            limit: 255
    t.string   "plastic_bag_width",             limit: 255
    t.string   "plastic_bag_height",            limit: 255
    t.string   "plastic_bag_weight",            limit: 255
    t.string   "external_box_length",           limit: 255
    t.string   "external_box_width",            limit: 255
    t.string   "external_box_height",           limit: 255
    t.string   "external_box_weight",           limit: 255
    t.string   "internal_box_length",           limit: 255
    t.string   "internal_box_width",            limit: 255
    t.string   "internal_box_height",           limit: 255
    t.string   "product_download_file_name",    limit: 255
    t.string   "product_download_content_type", limit: 255
    t.integer  "product_download_file_size",    limit: 4
    t.datetime "product_download_updated_at"
    t.integer  "priority",                      limit: 4,   default: 1,     null: false
  end

  add_index "products", ["slug"], name: "index_products_on_slug", unique: true, using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",              limit: 255,                 null: false
    t.string   "authorizable_type", limit: 255
    t.integer  "authorizable_id",   limit: 4
    t.boolean  "system",                        default: false, null: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  add_index "roles", ["authorizable_type", "authorizable_id"], name: "index_roles_on_authorizable_type_and_authorizable_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "roles_users", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4, null: false
    t.integer "role_id", limit: 4, null: false
  end

  add_index "roles_users", ["role_id"], name: "index_roles_users_on_role_id", using: :btree
  add_index "roles_users", ["user_id"], name: "index_roles_users_on_user_id", using: :btree

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", limit: 255,   null: false
    t.text     "data",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",               limit: 255,             null: false
    t.string   "email",              limit: 255,             null: false
    t.string   "crypted_password",   limit: 255,             null: false
    t.string   "password_salt",      limit: 255,             null: false
    t.string   "persistence_token",  limit: 255,             null: false
    t.string   "perishable_token",   limit: 255,             null: false
    t.integer  "login_count",        limit: 4,   default: 0, null: false
    t.integer  "failed_login_count", limit: 4,   default: 0, null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip",   limit: 255
    t.string   "last_login_ip",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

  add_foreign_key "notice_images", "notices"
  add_foreign_key "product_images", "products"
end
