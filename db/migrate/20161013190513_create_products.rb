class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.integer :product_line_id
      t.integer :product_category_id
      t.string :reference
      t.string :title
      t.text :description
      t.string :ean
      t.string :product_length
      t.string :product_width
      t.string :product_height
      t.string :product_weight
      t.string :product_parts
      t.string :product_colors
      t.string :product_minimum_age
      t.string :product_packed_length
      t.string :product_packed_width
      t.string :product_packed_height
      t.string :product_packed_weight
      t.string :product_packed_quantity
      t.boolean :new,:null => false, :default => 0
      t.boolean :featured,:null => false, :default => 0
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.string :slug
      t.timestamps
    end
    add_index :products, :slug, unique: true
  end

  def self.down
    drop_table :products
  end
end
