class CreateProductCategories < ActiveRecord::Migration
  def self.up
    create_table :product_categories do |t|
      t.integer :product_line_id
      t.string :title
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.string :slug
      t.timestamps
    end
    add_index :product_categories, :slug, unique: true
    ProductCategory.create_translation_table! :title => :string, :slug => :string
  end

  def self.down
    drop_table :product_categories
    ProductCategory.drop_translation_table!
  end
end
