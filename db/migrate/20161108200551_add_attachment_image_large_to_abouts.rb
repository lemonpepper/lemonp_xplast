class AddAttachmentImageLargeToAbouts < ActiveRecord::Migration
  def self.up
    change_table :abouts do |t|
      t.attachment :image_large
    end
  end

  def self.down
    remove_attachment :abouts, :image_large
  end
end
