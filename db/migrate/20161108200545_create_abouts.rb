class CreateAbouts < ActiveRecord::Migration
  def self.up
    create_table :abouts do |t|
      t.string :title_about
      t.text :description_about
      t.string :title_newsletter
      t.text :description_newsletter
      t.string :movie_link
      t.string :title_movie
      t.text :description_movie
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
    About.create_translation_table! :title_about => :string, :description_about => :text, :title_newsletter => :string, :description_newsletter => :text, :title_movie => :string, :description_movie => :text
  end

  def self.down
    drop_table :abouts
    About.drop_translation_table!
  end
end
