class AddComplementsToAbout < ActiveRecord::Migration
  def change
    reversible do |dir|
      dir.up do
        add_column :abouts, :numbers_injectors, :string
        add_column :abouts, :title_injectors, :string
        add_column :abouts, :subtitle_injectors, :string
        add_column :abouts, :title_tooling, :string
        add_column :abouts, :subtitle_tooling, :string
        add_column :abouts, :title_metallization, :string
        add_column :abouts, :subtitle_metallization, :string
        About.add_translation_fields! :title_injectors => :string, :subtitle_injectors => :string, :title_tooling => :string, :subtitle_tooling => :string, :title_metallization => :string, :subtitle_metallization => :string
      end

      dir.down do 
        remove_column :abouts, :title_injectors
        remove_column :abouts, :subtitle_injectors
        remove_column :abouts, :title_tooling
        remove_column :abouts, :subtitle_tooling
        remove_column :abouts, :title_metallization
        remove_column :abouts, :subtitle_metallization
        
        remove_column :about_translations, :title_injectors
        remove_column :about_translations, :subtitle_injectors
        remove_column :about_translations, :title_tooling
        remove_column :about_translations, :subtitle_tooling
        remove_column :about_translations, :title_metallization
        remove_column :about_translations, :subtitle_metallization 
      end
    end

  end
end
