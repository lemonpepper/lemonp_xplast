class AddEventsToNotice < ActiveRecord::Migration
  def change
    add_column :notices, :its_an_event, :boolean,:null => false, :default => 0
    add_column :notices, :start_event, :date
    add_column :notices, :stop_event, :date
  end
end
