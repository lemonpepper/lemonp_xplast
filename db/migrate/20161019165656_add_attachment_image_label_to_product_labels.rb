class AddAttachmentImageLabelToProductLabels < ActiveRecord::Migration
  def self.up
    change_table :product_labels do |t|
      t.attachment :image_label
    end
  end

  def self.down
    remove_attachment :product_labels, :image_label
  end
end
