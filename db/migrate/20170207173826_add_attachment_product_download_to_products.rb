class AddAttachmentProductDownloadToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.attachment :product_download
    end
  end

  def self.down
    remove_attachment :products, :product_download
  end
end
