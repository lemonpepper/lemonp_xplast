class AddComplementsToConfiguration < ActiveRecord::Migration
  def change
    reversible do |dir|
      dir.up do
        add_column :configurations, :title_packed_product_specifications_pack, :string
        add_column :configurations, :title_parts, :string
        add_column :configurations, :title_nothing_found, :string
        add_column :configurations, :title_phone, :string
        add_column :configurations, :title_address, :string
        add_column :configurations, :title_neighborhood, :string
        add_column :configurations, :title_city, :string
        add_column :configurations, :title_email, :string
        add_column :configurations, :title_site, :string
        add_column :configurations, :title_service_text, :string
        add_column :configurations, :title_currency_exchange, :string
        add_column :configurations, :title_know_more, :string
        add_column :configurations, :title_news, :string
        add_column :configurations, :title_events, :string
        
        Configuration.add_translation_fields! :title_packed_product_specifications_pack => :string, :title_parts => :string, :title_nothing_found => :string, :title_phone => :string, :title_address => :string, :title_neighborhood => :string, :title_city => :string, :title_email => :string, :title_site => :string, :title_service_text => :string, :title_currency_exchange => :string, :title_know_more => :string, :title_news => :string, :title_events => :string
      end
      
      dir.down do 
        remove_column :configurations, :title_packed_product_specifications_pack
        remove_column :configurations, :title_parts
        remove_column :configurations, :title_nothing_found
        remove_column :configurations, :title_phone
        remove_column :configurations, :title_address
        remove_column :configurations, :title_neighborhood
        remove_column :configurations, :title_city
        remove_column :configurations, :title_email
        remove_column :configurations, :title_site
        remove_column :configurations, :title_service_text
        remove_column :configurations, :title_currency_exchange
        remove_column :configurations, :title_know_more
        remove_column :configurations, :title_news
        remove_column :configurations, :title_events
        
        remove_column :configuration_translations, :title_packed_product_specifications_pack
        remove_column :configuration_translations, :title_parts
        remove_column :configuration_translations, :title_nothing_found
        remove_column :configuration_translations, :title_phone
        remove_column :configuration_translations, :title_address
        remove_column :configuration_translations, :title_neighborhood
        remove_column :configuration_translations, :title_city
        remove_column :configuration_translations, :title_email
        remove_column :configuration_translations, :title_site
        remove_column :configuration_translations, :title_service_text
        remove_column :configuration_translations, :title_currency_exchange
        remove_column :configuration_translations, :title_know_more
        remove_column :configuration_translations, :title_news
        remove_column :configuration_translations, :title_events
      end
    end
  end
end
