class CreateNoticeImages < ActiveRecord::Migration
  def self.up
    create_table :notice_images do |t|
      t.references :notice, index: true, foreign_key: true
      t.string :title
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
    NoticeImage.create_translation_table! :title => :string
  end

  def self.down
    drop_table :notice_images
    NoticeImage.drop_translation_table!
  end
end
