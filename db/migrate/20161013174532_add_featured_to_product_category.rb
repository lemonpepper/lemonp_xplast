class AddFeaturedToProductCategory < ActiveRecord::Migration
  def change
    add_column :product_categories, :featured, :boolean
  end
end
