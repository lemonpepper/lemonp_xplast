class AddAttachmentImageNoticeToNoticeImages < ActiveRecord::Migration
  def self.up
    change_table :notice_images do |t|
      t.attachment :image_notice
    end
  end

  def self.down
    remove_attachment :notice_images, :image_notice
  end
end
