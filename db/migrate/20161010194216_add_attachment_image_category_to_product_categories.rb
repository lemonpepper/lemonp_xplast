class AddAttachmentImageCategoryToProductCategories < ActiveRecord::Migration
  def self.up
    change_table :product_categories do |t|
      t.attachment :image_category
    end
  end

  def self.down
    remove_attachment :product_categories, :image_category
  end
end
