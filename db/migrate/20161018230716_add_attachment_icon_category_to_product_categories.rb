class AddAttachmentIconCategoryToProductCategories < ActiveRecord::Migration
  def self.up
    change_table :product_categories do |t|
      t.attachment :icon_category
    end
  end

  def self.down
    remove_attachment :product_categories, :icon_category
  end
end
