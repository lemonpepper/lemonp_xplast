class AddAttachmentCatalogFurnitureToConfigurations < ActiveRecord::Migration
  def self.up
    change_table :configurations do |t|
      t.attachment :catalog_furniture
    end
  end

  def self.down
    remove_attachment :configurations, :catalog_furniture
  end
end
