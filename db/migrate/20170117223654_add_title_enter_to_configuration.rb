class AddTitleEnterToConfiguration < ActiveRecord::Migration
  def change
    reversible do |dir|
      dir.up do
        add_column :configurations, :title_enter, :string
        Configuration.add_translation_fields! :title_enter => :string
      end

      dir.down do 
        remove_column :configurations, :title_enter        
        remove_column :configuration_translations, :title_enter
      end
    end

  end
  
end
