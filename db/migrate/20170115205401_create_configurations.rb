class CreateConfigurations < ActiveRecord::Migration
  def self.up
    create_table :configurations do |t|
      t.string :title_telesales
      t.string :title_contact
      t.string :title_institutional
      t.string :title_virtual_catalog
      t.string :title_download_virtual_catalog
      t.string :text_download_virtual_catalog
      t.string :title_download
      t.string :title_search
      t.string :title_more
      t.string :title_product_data
      t.string :title_length
      t.string :title_width
      t.string :title_height
      t.string :title_weight
      t.string :title_colors
      t.string :title_description
      t.string :title_product_specifications
      t.string :title_quantity
      t.string :title_colors
      t.string :title_see_the_product
      t.string :title_production_xplast
      t.string :title_contact_us
      t.string :phone_number
      t.string :sac_number
      t.text :description_about
      t.text :description_distributed
      t.string :title_assistance
      t.string :sac_mail
      t.string :link_facebook
      t.string :link_instagram
      t.string :link_linkedin
      
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
    Configuration.create_translation_table! :title_telesales => :string, :title_contact => :string, :title_institutional => :string, :title_virtual_catalog => :string, :title_download_virtual_catalog => :string, :text_download_virtual_catalog => :string, :title_download => :string, :title_search => :string, :title_more => :string, :title_product_data => :string, :title_length => :string, :title_width => :string, :title_height => :string, :title_weight => :string, :title_colors => :string, :title_description => :string, :title_product_specifications => :string, :title_quantity => :string, :title_colors => :string, :title_see_the_product => :string, :title_production_xplast => :string, :title_contact_us => :string, :phone_number => :string, :sac_number => :string, :description_about => :text, :description_distributed => :text, :title_assistance => :string
  end

  def self.down
    drop_table :configurations
    Configuration.drop_translation_table!
  end
end
