class AddMovieLinkToProduct < ActiveRecord::Migration
  def change
    add_column :products, :movie_link, :string
  end
end
