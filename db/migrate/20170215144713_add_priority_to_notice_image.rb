class AddPriorityToNoticeImage < ActiveRecord::Migration
  def change
    add_column :notice_images, :priority, :integer, :null => false, :default => 1
  end
end
