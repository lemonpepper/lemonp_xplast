class CreateNotices < ActiveRecord::Migration
  def self.up
    create_table :notices do |t|
      t.string :title
      t.text :description
      t.string :title_source
      t.string :slug
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
    add_index :notices, :slug, unique: true
    Notice.create_translation_table! :title => :string, :description => :text
  end

  def self.down
    drop_table :notices
    Notice.drop_translation_table!
  end
end
