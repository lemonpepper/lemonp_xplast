class AddProductLabelIdToProduct < ActiveRecord::Migration
  def change
    add_column :products, :product_label_id, :integer
  end
end
