class AddPriorityToProduct < ActiveRecord::Migration
  def change
    add_column :products, :priority, :integer, :null => false, :default => 1
  end
end
