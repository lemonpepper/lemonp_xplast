class AddAttachmentImageIconToAbouts < ActiveRecord::Migration
  def self.up
    change_table :abouts do |t|
      t.attachment :image_icon
    end
  end

  def self.down
    remove_attachment :abouts, :image_icon
  end
end
