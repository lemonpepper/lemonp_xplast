class AddAttachmentCatalogDomesticUtilityToConfigurations < ActiveRecord::Migration
  def self.up
    change_table :configurations do |t|
      t.attachment :catalog_domestic_utility
    end
  end

  def self.down
    remove_attachment :configurations, :catalog_domestic_utility
  end
end
