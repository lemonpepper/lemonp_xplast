class AddClassToysToProductCategories < ActiveRecord::Migration
  def change
    add_column :product_categories, :class_toys, :string
  end
end
