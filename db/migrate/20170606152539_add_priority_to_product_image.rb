class AddPriorityToProductImage < ActiveRecord::Migration
  def change
    add_column :product_images, :priority, :integer, :null => false, :default => 2
  end
end
