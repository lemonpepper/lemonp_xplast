class AddAttachmentCatalogToysToConfigurations < ActiveRecord::Migration
  def self.up
    change_table :configurations do |t|
      t.attachment :catalog_toys
    end
  end

  def self.down
    remove_attachment :configurations, :catalog_toys
  end
end
