class AddAttachmentImageMetallizationToAbouts < ActiveRecord::Migration
  def self.up
    change_table :abouts do |t|
      t.attachment :image_metallization
    end
  end

  def self.down
    remove_attachment :abouts, :image_metallization
  end
end
