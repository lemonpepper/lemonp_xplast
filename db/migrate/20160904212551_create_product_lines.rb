class CreateProductLines < ActiveRecord::Migration
  def self.up
    create_table :product_lines do |t|
      t.string :title
      t.string :short_description
      t.text :description
      t.string :slug
      t.boolean :published, null: false, default: 1
      t.timestamps
    end
    add_index :product_lines, :slug, unique: true
    ProductLine.create_translation_table! :title => :string, :short_description => :string, :description => :text
  end

  def self.down
    drop_table :product_lines
    ProductLine.drop_translation_table!
  end
end
