class AddPriorityToProductCategory < ActiveRecord::Migration
  def change
    add_column :product_categories, :priority, :integer, :null => false, :default => 1
  end
end
