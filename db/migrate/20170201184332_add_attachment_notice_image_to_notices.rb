class AddAttachmentNoticeImageToNotices < ActiveRecord::Migration
  def self.up
    change_table :notices do |t|
      t.attachment :notice_image
    end
  end

  def self.down
    remove_attachment :notices, :notice_image
  end
end
