class AddLinkSourceToNotice < ActiveRecord::Migration
  def change
    add_column :notices, :link_source, :string
  end
end
