class AddComplementsShareToConfiguration < ActiveRecord::Migration
  
  def change
    reversible do |dir|
      dir.up do
        add_column :configurations, :title_download_images, :string
        add_column :configurations, :title_source_news, :string
        add_column :configurations, :title_send_button, :string
        add_column :configurations, :title_share, :string
        
        Configuration.add_translation_fields! :title_download_images => :string, :title_source_news => :string, :title_send_button => :string, :title_share => :string
      end
      
      dir.down do 
        remove_column :configurations, :title_download_images, :string
        remove_column :configurations, :title_source_news, :string
        remove_column :configurations, :title_send_button, :string
        remove_column :configurations, :title_share, :string
        
        remove_column :configuration_translations, :title_download_images, :string
        remove_column :configuration_translations, :title_source_news, :string
        remove_column :configuration_translations, :title_send_button, :string
        remove_column :configuration_translations, :title_share, :string
      end
    end
  end
end
