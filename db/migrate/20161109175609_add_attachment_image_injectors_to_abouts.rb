class AddAttachmentImageInjectorsToAbouts < ActiveRecord::Migration
  def self.up
    change_table :abouts do |t|
      t.attachment :image_injectors
    end
  end

  def self.down
    remove_attachment :abouts, :image_injectors
  end
end
