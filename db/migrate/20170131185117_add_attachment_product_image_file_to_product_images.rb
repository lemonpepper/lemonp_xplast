class AddAttachmentProductImageFileToProductImages < ActiveRecord::Migration
  def self.up
    change_table :product_images do |t|
      t.attachment :product_image_file
    end
  end

  def self.down
    remove_attachment :product_images, :product_image_file
  end
end
