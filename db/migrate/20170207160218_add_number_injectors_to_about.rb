class AddNumberInjectorsToAbout < ActiveRecord::Migration
  def change
    add_column :abouts, :number_injectors, :string
  end
end
