class AddAttachmentImageSmallToAbouts < ActiveRecord::Migration
  def self.up
    change_table :abouts do |t|
      t.attachment :image_small
    end
  end

  def self.down
    remove_attachment :abouts, :image_small
  end
end
