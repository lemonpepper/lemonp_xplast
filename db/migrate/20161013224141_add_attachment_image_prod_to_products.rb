class AddAttachmentImageProdToProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.attachment :image_prod
    end
  end

  def self.down
    remove_attachment :products, :image_prod
  end
end
