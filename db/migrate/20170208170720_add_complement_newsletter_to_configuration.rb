class AddComplementNewsletterToConfiguration < ActiveRecord::Migration
  def change
    add_column :configurations, :newsletter_success, :string
    add_column :configurations, :newsletter_error, :string
  end
  
  def change
    reversible do |dir|
      dir.up do
        add_column :configurations, :newsletter_success, :string
        add_column :configurations, :newsletter_error, :string
        
        Configuration.add_translation_fields! :newsletter_success => :string, :newsletter_error => :string
      end
      
      dir.down do 
        remove_column :configurations, :newsletter_success, :string
        remove_column :configurations, :newsletter_error, :string

        
        remove_column :configuration_translations, :newsletter_success, :string
        remove_column :configuration_translations, :newsletter_error, :string
      end
    end
  end
  
  
end
