class AddComplementsUdToProduct < ActiveRecord::Migration
  def change
    add_column :products, :cubage, :string
    add_column :products, :unit, :string
    add_column :products, :packing, :string
    add_column :products, :dun, :string
    add_column :products, :cnm, :string
    add_column :products, :plastic_bag_length, :string
    add_column :products, :plastic_bag_width, :string
    add_column :products, :plastic_bag_height, :string
    add_column :products, :plastic_bag_weight, :string
    add_column :products, :external_box_length, :string
    add_column :products, :external_box_width, :string
    add_column :products, :external_box_height, :string
    add_column :products, :external_box_weight, :string
    add_column :products, :internal_box_length, :string
    add_column :products, :internal_box_width, :string
    add_column :products, :internal_box_height, :string
  end
end
