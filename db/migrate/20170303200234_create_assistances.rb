class CreateAssistances < ActiveRecord::Migration
  def self.up
    create_table :assistances do |t|
      t.string :state
      t.string :name
      t.string :phone
      t.string :address
      t.string :district
      t.string :city
      t.string :zip_code
      t.string :email
      t.string :site
      t.text :observation
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :assistances
  end
end
