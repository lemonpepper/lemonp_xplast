class AddAttachmentImageToolingToAbouts < ActiveRecord::Migration
  def self.up
    change_table :abouts do |t|
      t.attachment :image_tooling
    end
  end

  def self.down
    remove_attachment :abouts, :image_tooling
  end
end
