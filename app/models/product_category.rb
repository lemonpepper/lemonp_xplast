class ProductCategory < ActiveRecord::Base
  #attr_accessible :product_line_id, :title
  translates :title, :slug, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations
  globalize_accessors :locales => I18n.available_locales, :attributes => translated_attribute_names
  
  has_attached_file :image_category, styles: { medium: "300x300>", thumb: "50x50>", home: "293×700>"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :image_category, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :icon_category, styles: {thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :icon_category, :content_type => /\Aimage\/.*\Z/  
  
  extend FriendlyId
  friendly_id :title, use: [:slugged, :globalize, :finders]
  
  belongs_to :product_line
  has_many :products
  #has_many :product_lines
end
