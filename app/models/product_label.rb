class ProductLabel < ActiveRecord::Base
  #attr_accessible :title  
  has_attached_file :image_label, styles: {thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :image_label, :content_type => /\Aimage\/.*\Z/
  
  validates :image_label, presence: true
  
  has_many :products
end
