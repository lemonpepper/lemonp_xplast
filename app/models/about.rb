class About < ActiveRecord::Base
  #attr_accessible :title_about, :description_about, :title_newsletter, :description_newsletter, :movie_link, :title_movie, :description_movie
  
  translates :title_about, :description_about, :title_newsletter, :description_newsletter, :title_movie, :description_movie, :title_injectors, :subtitle_injectors, :title_tooling, :subtitle_tooling, :title_metallization, :subtitle_metallization, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations
  globalize_accessors :locales => I18n.available_locales, :attributes => translated_attribute_names
  
  
  has_attached_file :image_small, styles: {thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :image_small, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :image_large, styles: {thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :image_large, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :image_icon, styles: {thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :image_icon, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :image_injectors, styles: {thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :image_injectors, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :image_tooling, styles: {thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :image_tooling, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :image_metallization, styles: {thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :image_metallization, :content_type => /\Aimage\/.*\Z/

end
