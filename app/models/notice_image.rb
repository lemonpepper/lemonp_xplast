class NoticeImage < ActiveRecord::Base
  #attr_accessible :title
  
  translates :title, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations
  globalize_accessors :locales => I18n.available_locales, :attributes => translated_attribute_names    
  
  has_attached_file :image_notice, styles: {thumb: "150x150#", internal: "300x300#", big: "800×800>"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :image_notice, :content_type => /\Aimage\/.*\Z/
  validates_presence_of :image_notice
  
  belongs_to :notice
  
  default_scope { order("priority ASC, id") }
end
