class Configuration < ActiveRecord::Base
  #attr_accessible :title_telesales, :title_contact, :title_institutional, :title_virtual_catalog, :title_download_virtual_catalog, :text_download_virtual_catalog, :title_download, :title_search, :title_more, :title_product_data, :title_length, :title_width, :title_height, :title_weight, :title_colors, :title_description, :title_product_specifications, :title_quantity, :title_colors, :title_see_the_product, :title_production_xplast, :title_contact_us
  
  translates :title_telesales, :title_contact, :title_institutional, :title_virtual_catalog, :title_download_virtual_catalog, :text_download_virtual_catalog, :title_download, :title_enter, :title_search, :title_more, :title_product_data, :title_length, :title_width, :title_height, :title_weight, :title_colors, :title_description, :title_product_specifications, :title_quantity, :title_see_the_product, :title_production_xplast, :title_contact_us, :phone_number, :sac_number, :description_about, :description_distributed, :title_assistance, :title_packed_product_specifications_pack, :title_parts, :title_nothing_found, :title_phone, :title_address, :title_neighborhood, :title_city, :title_email, :title_site, :title_service_text, :title_currency_exchange, :title_know_more, :title_news, :title_events,:title_download_images, :title_source_news, :title_send_button, :title_share, :newsletter_success,  :newsletter_error, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations
  globalize_accessors :locales => I18n.available_locales, :attributes => translated_attribute_names
  
  
  has_attached_file :catalog_furniture
  do_not_validate_attachment_file_type :catalog_furniture
  
  has_attached_file :catalog_toys
  do_not_validate_attachment_file_type :catalog_toys
  
  has_attached_file :catalog_domestic_utility
  do_not_validate_attachment_file_type :catalog_domestic_utility
  
end