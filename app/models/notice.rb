class Notice < ActiveRecord::Base
  #attr_accessible :title, :description, :title_source
  
  translates :title, :description, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations
  globalize_accessors :locales => I18n.available_locales, :attributes => translated_attribute_names
  
  has_attached_file :notice_image, styles: { internal: "300x300>", thumb: "50x50>", home: "555x226>"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :notice_image, :content_type => /\Aimage\/.*\Z/
  
  
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  
  has_many :notice_images
  
  # Metodo para listar todos publicados
  def self.all_published(ordernar ='', limit = '')
    order_show = 'id'
    if !ordernar.blank? 
      order_show = ordernar
    end
    if !limit.blank?    
      where(active: true, published: true).order(order_show).limit(limit)
    else
      where(active: true, published: true).order(order_show)
    end
  end
  
  def self.all_events_published(ordernar ='', limit = '')
    order_show = 'id'
    if !ordernar.blank? 
      order_show = ordernar
    end
    if !limit.blank?    
      where("`notices`.`active` = 1 AND `notices`.`published` = 1 AND `notices`.`its_an_event` = 1 AND `notices`.`stop_event` >= '#{Date.today}'").order(order_show).limit(limit)
    else
      where("`notices`.`active` = 1 AND `notices`.`published` = 1 AND `notices`.`its_an_event` = 1 AND `notices`.`stop_event` >= '#{Date.today}'").order(order_show)
    end
  end
end
