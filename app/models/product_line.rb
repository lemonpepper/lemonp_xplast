class ProductLine < ActiveRecord::Base
  #attr_accessible :title, :short_description, :description, :slug, :published
  
  translates :title, :short_description, :description, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations
  globalize_accessors :locales => I18n.available_locales, :attributes => translated_attribute_names
  has_attached_file :product_line_image, styles: { medium: "300x300>", thumb: "50x50>" }
  validates_attachment_content_type :product_line_image, :content_type => /\Aimage\/.*\Z/
  
  has_many :product_categories
  has_many :products
  #belongs_to :product_category
end
