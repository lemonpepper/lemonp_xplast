class ProductImage < ActiveRecord::Base
  #attr_accessible
  
  has_attached_file :product_image_file, styles: {thumb: "50x50>", internal: "609×609>", big: "800×800>", internal_thumb: "130x130>", medium: "306x306>"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :product_image_file, :content_type => /\Aimage\/.*\Z/
  validates_presence_of :product_image_file
  belongs_to :product
end
