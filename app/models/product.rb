class Product < ActiveRecord::Base
  #attr_accessible :product_line_id, :product_category_id, :reference, :title, :description, :ean, :product_length, :product_width, :product_height, :product_weight, :product_parts, :product_colors, :product_minimum_age, :product_packed_length, :product_packed_width, :product_packed_height, :product_packed_weight, :product_packed_quantity, :new, :featured, :slug, :movie_link, :cubage, :unit, :packing, :dun, :cnm, :plastic_bag_length, :plastic_bag_width,  :plastic_bag_height, :plastic_bag_weight, :external_box_length, :external_box_width,  :external_box_height, :external_box_weight, :internal_box_length, :internal_box_width, :internal_box_height
  translates :title, :description, :product_colors, :fallbacks_for_empty_translations => true
  accepts_nested_attributes_for :translations
  globalize_accessors :locales => I18n.available_locales, :attributes => translated_attribute_names
  
  has_attached_file :image_prod, styles: { medium: "306x306>", thumb: "50x50>", internal: "609×609>", big: "800×800>", ud_home: "263x181>" , internal_thumb: "130x130>"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :image_prod, :content_type => /\Aimage\/.*\Z/
  
  
  has_attached_file :product_download
  do_not_validate_attachment_file_type :product_download
  
  
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  
  belongs_to :product_line
  belongs_to :product_category
  belongs_to :product_label
  
  has_many :product_images
  
  def with_translations(*locales)
    locales = translated_locales if locales.empty?
    includes(:translations).with_locales(locales).with_required_attributes
  end
  
  def self.search(search)
   with_translations.where("title LIKE ? OR description LIKE ? OR reference LIKE ? OR ean LIKE ?", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%").includes(:translations)
  end
end
