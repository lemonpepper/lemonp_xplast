module ApplicationHelper

  def language_links
    links = []
    I18n.available_locales.each do |locale|
      locale_key = "translation.#{locale}"
      if locale == I18n.locale
        links << "<li>" + link_to(image_tag("flag-#{locale.to_s}.jpg"), "#", :class=> "btn_language disabled") + "</li>"
      else
        links << "<li>" + link_to(image_tag("flag-#{locale.to_s}.jpg"), url_for(:locale => locale.to_s), :class=> "btn_language") + "</li>"
      end
    end
    links.join("\n").html_safe
  end
  
  
  def translate_option(language)
    case language.to_s
      when "pt-BR" then return t("translation.pt-BR")
      when "en" then return t("translation.en")
      when "es" then return t("translation.es")
    else
      return "Deu merda"
    end
  end 
  
  def show_boolean(value)
    if value
      return "Sim"
    else
      return "Não"
    end
  end
  
end
