$(document).ready(function(){
	var myClasses = new Array();
	$("#representative ul li").hide();
	$("#representative ul li").each(function() {
		txt = $(this).attr("class");
		atC = txt.split(" ");
		var obj = "";

		for(i=0;i<atC.length;i++) {
			obj = "#"+atC[i];
			$(obj).addClass("possui");
		}
	});

	$("path.possui").click(function() {
		$("path").removeClass("sobre");
		$(this).addClass("sobre");

		$("#representative ul li").hide();
		$("#representative ul li."+$(this).attr("id")).show();
	});
});