class DomesticUtilities::HomesController < ApplicationController
  layout "ud"
  before_filter :load_configurations
  def index
    @product_categories = load_categories(3)
    @product_featureds = Product.where(active: true, published: true, featured: true, product_line: 3)
    @products = Product.where(active: true, published: true, product_line_id: 3)
  end 
  
  def search
    @product_categories = load_categories(3) 
    if !params[:term].blank?
      @products = Product.search(params[:term])
    end
  end
end
