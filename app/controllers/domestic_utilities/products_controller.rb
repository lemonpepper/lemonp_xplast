class DomesticUtilities::ProductsController < ApplicationController
  layout "ud"
  before_filter :load_configurations
  def show
    @product_categories = load_categories(3)
    @product = Product.find(params[:id])
    @product_images = @product.product_images.all_published
  end
end
