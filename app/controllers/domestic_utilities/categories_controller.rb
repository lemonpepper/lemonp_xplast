class DomesticUtilities::CategoriesController < ApplicationController
  layout "ud"
  before_filter :load_configurations
  def show
    @product_categories = load_categories(3)
    @product_category = ProductCategory.find(params[:id])
    @products = @product_category.products.all_published
  end
end
