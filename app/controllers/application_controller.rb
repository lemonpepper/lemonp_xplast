class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  helper_method :current_user_session, :current_user
  protect_from_forgery with: :exception
  
  private
    def current_user_session
      return @current_user_session if defined?(@current_user_session)
      @current_user_session = UserSession.find
    end

    def current_user
      return @current_user if defined?(@current_user)
      @current_user = current_user_session && current_user_session.user
    end
  
  
  rescue_from 'Acl9::AccessDenied', :with => :access_denied

  def access_denied
    if current_user
      redirect_to login_path, :notice => t("message_show.permission")      
    else
      redirect_to login_path, :notice => t("message_show.to_login")  
    end
  end
  
  def load_categories(category)
    return ProductCategory.where(product_line_id: category,active: true, published: true).order("priority ASC, id")
  end
  
  def load_configurations
    @conf = ::Configuration.find(1)
  end
  
  #require 'nokogiri'
  #require 'open-uri'
  # Lê os dados financeiros
  def show_currency_exchange(locale)
    begin
      convert_currency()
      if !@erro_google
        case locale      
          when "pt-BR"
            value1 = convert_currency("USD", "BRL")
            @value1 =  "<span class='title'>USD</span>#{value1.css(".bld")[0].text} <span class='#{def_class(value1.css(".bld")[1].text)}'>#{value1.css(".bld")[1].text}</span> "
            value2 = convert_currency("PYG", "BRL")
            @value2 =  "<span class='title'>PYG</span>#{value2.css(".bld")[0].text} <span class='#{def_class(value2.css(".bld")[1].text)}'>#{value2.css(".bld")[1].text}</span> "
          when "es"
            value1 = convert_currency("USD", "PYG")
            @value1 =  "<span class='title'>USD</span>#{value1.css(".bld")[0].text} <span class='#{def_class(value1.css(".bld")[1].text)}'>#{value1.css(".bld")[1].text}</span> "
            value2 = convert_currency("BRL", "PYG")
            @value2 =  "<span class='title'>BRL</span>#{value2.css(".bld")[0].text} <span class='#{def_class(value2.css(".bld")[1].text)}'>#{value2.css(".bld")[1].text}</span> "
          when "en"
            value1 = convert_currency("BRL", "USD")
            @value1 =  "<span class='title'>BRL</span>#{value1.css(".bld")[0].text} <span class='#{def_class(value1.css(".bld")[1].text)}'>#{value1.css(".bld")[1].text}</span> "
            value2 = convert_currency("PYG", "USD")
            @value2 =  "<span class='title'>PYG</span>#{value2.css(".bld")[0].text} <span class='#{def_class(value2.css(".bld")[1].text)}'>#{value2.css(".bld")[1].text}</span> "
        else
          return "Deu merda"
        end
      else
        @erro_loading = true
      end
    rescue
      @erro_loading = true
    ensure
      
    end   
  end

  def convert_currency(from_curr = "INR", to_curr = "USD")
    url = "http://www.google.com/finance?q=#{from_curr}#{to_curr}"
    begin
      return Nokogiri::HTML(open(url))
    rescue
      @erro_google = true
    ensure  
    
    end
  end
  
  def def_class (str)
    if str[0] == "-"
      return "finance_down"
    else
      return "finance_up"
    end
  end
  # Termina de ler os dados financeiros
  
  
  protected

  def handle_unverified_request
    # raise an exception
    fail ActionController::InvalidAuthenticityToken
    # or destroy session, redirect
    if current_user_session
      current_user_session.destroy
    end
    redirect_to root_url
  end
    
end
