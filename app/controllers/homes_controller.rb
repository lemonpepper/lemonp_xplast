class HomesController < ApplicationController
  layout "blank"
  before_filter :load_configurations
  
  def index
    @about = About.find(1)
    @product_lines = ProductLine.all
    show_currency_exchange(params[:locale])
    @notices = Notice.all_published('created_at desc',4)
    @events = Notice.all_events_published()
  end
  
  def about
    @about = About.find(1)
    @newsletter = Newsletter.new
  end
  
  def assistance
    @assistances = Assistance.all_published("state, name")
  end 

  def load_image
    if params[:prod].blank? && params[:image_prod].blank?
      @erro = true
    else
      if !params[:prod].blank?
        product = Product.find_by_id(params[:prod])
        @image_internal = product.image_prod.url("internal")
        @image_big = product.image_prod.url("big")
      else
        product = ProductImage.find_by_id(params[:image_prod])
        @image_internal = product.product_image_file.url("internal")
        @image_big = product.product_image_file.url("big")
      end
    end
  end
  
end
