class UserSessionsController < ApplicationController
  layout "login"
  def new
    @user_session = UserSession.new
  end

  def create
    puts params[:user_session]
    @user_session = UserSession.new(ad_params)
    #@user_session = UserSession.new(:email => "bsilva@lemonp.com.br", :password => "123123123", :remember_me => true)
    if @user_session.save
      redirect_to admin_root_url, :notice => t("message_show.login") 
    else
      render :action => 'new'
    end
  end

  def destroy
    current_user_session.destroy
    redirect_to login_path, :notice => t("message_show.logout") 
  end
  
  private

  def ad_params
    params.require(:user_session).permit(:email, :password)
  end
end
