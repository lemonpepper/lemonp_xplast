class Admin::ConfigurationsController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @configurations = ::Configuration.all_active
  end

  def show
    @configuration = ::Configuration.find(params[:id])
  end

  def new
    @configuration = ::Configuration.new
  end

  def create
    params.permit!
    @configuration = ::Configuration.new(params[:configuration])
    if @configuration.save
      redirect_to [:admin, @configuration], :notice => "Successfully created configuration."
    else
      render :action => 'new'
    end
  end

  def edit
    @configuration = ::Configuration.find(1)
  end

  def update
    params.permit!
    @configuration = ::Configuration.find(1)
    if @configuration.update_attributes(params[:configuration])
      redirect_to edit_admin_configuration_path("xplast"), :notice  => "Successfully updated configuration."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @configuration = ::Configuration.find(params[:id])
    @configuration.newdestroy
    redirect_to admin_configurations_url, :notice => "Successfully destroyed configuration."
  end
end
