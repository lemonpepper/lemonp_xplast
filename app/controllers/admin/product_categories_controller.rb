class Admin::ProductCategoriesController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  before_filter :load_product_line
  def index
    @product_categories = @product_line.product_categories.all_active("priority ASC")
  end

  def show
    @product_category = @product_line.product_categories.find(params[:id])
  end

  def new
    @product_category = @product_line.product_categories.build
  end

  def create
    params.permit!
    @product_category = @product_line.product_categories.build(params[:product_category])
    if @product_category.save
      redirect_to [:admin, @product_line, @product_category], :notice => "Successfully created product category."
    else
      render :action => 'new'
    end
  end

  def edit
    @product_category = @product_line.product_categories.find_by_slug(params[:id])
  end

  def update
    params.permit!
    @product_category = @product_line.product_categories.find_by_slug(params[:id])
    if @product_category.update_attributes(params[:product_category])
      redirect_to [:admin, @product_line, @product_category], :notice  => "Successfully updated product category."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @product_category = @product_line.product_categories.find_by_slug(params[:id])
    @product_category.newdestroy
    redirect_to admin_product_line_product_categories_path(@product_line), :notice => "Successfully destroyed product category."
  end
  
  def load_product_line
    @product_line = ProductLine.find(params[:product_line_id])
  end
end
