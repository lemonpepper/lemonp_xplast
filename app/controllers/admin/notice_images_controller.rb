class Admin::NoticeImagesController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  before_filter :load_notice
  def index
    @notice_images = @notice.notice_images.all_active
  end

  def show
    @notice_image = @notice.notice_images.find(params[:id])
  end

  def new
    @notice_image = @notice.notice_images.build
  end

  def create
    if !params[:notice_image][:attachments_array].blank?    
      params[:notice_image][:attachments_array].each do |file|
        
        params.permit!
        image_save = @notice.notice_images.build(:image_notice => file)
        image_save.save
      end
      redirect_to admin_notice_notice_images_path(@notice)
    else
      render :action => 'new'
    end   
    #@notice_image = @notice.notice_images.build(params[:notice_image])
    #if @notice_image.save
    #  redirect_to [:admin, @notice, @notice_image], :notice => "Successfully created notice image."
    #else
    #  render :action => 'new'
    #end
  end

  def edit
    @notice_image = @notice.notice_images.find(params[:id])
  end

  def update
    params.permit!
    @notice_image = @notice.notice_images.find(params[:id])
    if @notice_image.update_attributes(params[:notice_image])
      redirect_to [:admin, @notice, @notice_image], :notice  => "Successfully updated notice image."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @notice_image = @notice.notice_images.find(params[:id])
    @notice_image.newdestroy
    redirect_to admin_notice_notice_images_path(@notice), :notice => "Successfully destroyed notice image."
  end
  
  
  def sort
    params[:order].each do |key,value|
      @notice.notice_images.find(value[:id]).update_attribute(:priority,value[:position])
    end
    render :nothing => true
  end
  
  
  def unpublished
   @notice_image = @notice.notice_images.find(params[:id])
   if !@notice_image.blank?
     @notice_image.published = false
     @notice_image.save
   end
  end
  
  def published
   @notice_image = @notice.notice_images.find(params[:id])
   if !@notice_image.blank?
     @notice_image.published = true
     @notice_image.save
   end
  end
  
  
  def load_notice
    @notice = Notice.find(params[:notice_id])
  end
end
