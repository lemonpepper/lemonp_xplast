class Admin::ProductLabelsController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @product_labels = ProductLabel.all_active
  end

  def show
    @product_label = ProductLabel.find(params[:id])
  end

  def new
    @product_label = ProductLabel.new
  end

  def create
    params.permit!
    @product_label = ProductLabel.new(params[:product_label])
    if @product_label.save
      redirect_to [:admin, @product_label], :notice => "Successfully created product label."
    else
      render :action => 'new'
    end
  end

  def edit
    @product_label = ProductLabel.find(params[:id])
  end

  def update
    params.permit!
    @product_label = ProductLabel.find(params[:id])
    if @product_label.update_attributes(params[:product_label])
      redirect_to [:admin, @product_label], :notice  => "Successfully updated product label."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @product_label = ProductLabel.find(params[:id])
    @product_label.newdestroy
    redirect_to admin_product_labels_url, :notice => "Successfully destroyed product label."
  end
  
  
  def check_images
    begin
      photo_path = "#{Rails.root}/public/upload_labels/{*.jpg,*.png}"

      Dir.glob(photo_path).entries.each do |e|
        file_show = File.basename(e).split('.')[0]
        file_show = file_show.split('-')[0]
        
        product =  ProductLabel.find_by_id(file_show)
        puts file_show  
        if !product.blank?
          if product.image_label.file?
            #product_image = product.product_images.build
            #product_image.image_product = File.open(e)
            #product_image.save
            #File.delete(e)
            #puts "Produto Existe"
          else
            params.permit!
            product.image_label = File.open(e)
            product.save
            File.delete(e)
            puts "Produto Existe"
          end
        else
          puts "Produto não Existe referencia #{file_show}"
        end 
      end
      
    rescue => ex
      puts ex.inspect
    end  
    redirect_to admin_product_labels_path()  
  end
  
end
