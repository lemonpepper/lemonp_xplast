class Admin::AssistancesController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @assistances = Assistance.all_active
  end

  def show
    @assistance = Assistance.find(params[:id])
  end

  def new
    @assistance = Assistance.new
  end

  def create
    params.permit!
    @assistance = Assistance.new(params[:assistance])
    if @assistance.save
      redirect_to [:admin, @assistance], :notice => "Successfully created assistance."
    else
      render :action => 'new'
    end
  end

  def edit
    @assistance = Assistance.find(params[:id])
  end

  def update
    params.permit!
    @assistance = Assistance.find(params[:id])
    if @assistance.update_attributes(params[:assistance])
      redirect_to [:admin, @assistance], :notice  => "Successfully updated assistance."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @assistance = Assistance.find(params[:id])
    @assistance.newdestroy
    redirect_to admin_assistances_url, :notice => "Successfully destroyed assistance."
  end
end
