class Admin::ProductImagesController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  before_filter :load_product_line_product
  def index
    @product_images = @product.product_images.all_active("priority ASC, id")
  end

  def show
    @product_image = @product.product_images.find(params[:id])
  end

  def new
    @product_image = @product.product_images.build
  end

  def create
    params.permit!
    @product_image = @product.product_images.build(params[:product_image])
    if @product_image.save
      redirect_to [:admin, @product_line, @product, @product_image], :notice => "Successfully created product image."
    else
      render :action => 'new'
    end
  end

  def edit
    @product_image = @product.product_images.find(params[:id])
  end

  def update
    params.permit!
    @product_image = @product.product_images.find(params[:id])
    if @product_image.update_attributes(params[:product_image])
      redirect_to [:admin, @product_line, @product, @product_image], :notice  => "Successfully updated product image."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @product_image = @product.product_images.find(params[:id])
    @product_image.destroy
    redirect_to admin_product_line_product_product_images_path(@product_line, @product), :notice => "Successfully destroyed product image."
  end
  
  def sort
    params[:order].each do |key,value|
      @product.product_images.find(value[:id]).update_attribute(:priority,value[:position])
    end
    render :nothing => true
  end
  
  def unpublished
   @product_image = @product.product_images.find(params[:id])
   if !@product_image.blank?
     @product_image.published = false
     @product_image.save
   end
  end
  
  def published
    @product_image = @product.product_images.find(params[:id])
    if !@product_image.blank?
      @product_image.published = true
      @product_image.save
    end
  end
  
  
  
  def load_product_line_product
    @product_line = ProductLine.find(params[:product_line_id])
    @product = @product_line.products.find(params[:product_id])
    
  end
end
