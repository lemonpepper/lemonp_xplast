class Admin::HomesController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @notices = Notice.all_published    
  end
end
