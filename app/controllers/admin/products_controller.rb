class Admin::ProductsController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  before_filter :load_product_line
  before_filter :load_info, :only => [:new, :edit]
  def index
    @products = @product_line.products.all_active("product_category_id ASC, priority ASC, reference ASC")
  end

  def show
    @product = @product_line.products.find(params[:id])
  end

  def new
    @product = @product_line.products.build
  end

  def create
    params.permit!
    @product = @product_line.products.build(params[:product])
    
    movie1 = @product.movie_link.split('src="')
    if !movie1[1].blank?
      movie2 = movie1[1].split('"')
      @product.movie_link = movie2[0]
    end
    
    if @product.save
      redirect_to [:admin, @product_line, @product], :notice => "Successfully created product."
    else
      render :action => 'new'
    end
  end

  def edit
    @product = @product_line.products.find(params[:id])
  end

  def update
    params.permit!
    @product = @product_line.products.find(params[:id])
    
    movie1 = params[:product][:movie_link].split('src="')
    if !movie1[1].blank?
      movie2 = movie1[1].split('"')
      params[:product][:movie_link] = movie2[0]
    end
    
    if @product.update_attributes(params[:product])
      redirect_to [:admin, @product_line, @product], :notice  => "Successfully updated product."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @product = @product_line.products.find(params[:id])
    @product.newdestroy
    redirect_to admin_product_line_products_path(@product_line), :notice => "Successfully destroyed product."
  end
  
  def load_product_line
    @product_line = ProductLine.find(params[:product_line_id])
  end
  
  def check_images
    begin
      photo_path = "#{Rails.root}/public/upload_images/{*.jpg,*.png}"

      Dir.glob(photo_path).entries.each do |e|
        file_show = File.basename(e).split('.')[0]
        file_show = file_show.split('---')[0]
        file_show.gsub!("-", "/")
        puts file_show
        product =  Product.where(reference: file_show, product_line_id: @product_line).limit(1)   
        if product.blank?
          file_show = File.basename(e).split('.')[0]
          file_show = file_show.split('-')[0]
          puts file_show        
          product =  Product.where(reference: file_show, product_line_id: @product_line).limit(1)        
        end
        if product.blank?
          file_show = File.basename(e).split('.')[0]
          file_show = file_show.split('_')[0]
          puts file_show        
          product =  Product.where(reference: file_show, product_line_id: @product_line).limit(1)
        end 
        if !product.blank?
          if (((Time.now.to_f - product.first.updated_at.to_f)/86400).to_i < 5) 
            #Insere nas imagens de produto             
            puts "Produto: #{product}"
            params.permit!
            product_image = product.first.product_images.build(product_image_file: File.open(e))
            product_image.save
            File.delete(e)
            puts "Produto Cadastrado em Imagens de Produto"  
          else
              puts "Produto: #{product}"
              product.first.product_images.all.each do|imagens_off|
                imagens_off.published = false
                imagens_off.save
              end 
              params.permit!
              product_image = product.first.product_images.build(product_image_file: File.open(product.first.image_prod.path), priority: 1, published: false)
              product_image.save
              product.first.image_prod = nil
              product.first.active = true
              product.first.published = true
              product.first.save
              puts "Imagem do Produto cadastrada em Imagens de Produto"
              params.permit!
              product_image = product.first.product_images.build(product_image_file: File.open(e))
              product_image.save
              File.delete(e) 
              puts "Produto Cadastrado em Imagens de Produto"
          end
        else
          puts "Produto não Existe referencia #{file_show}"
        end 
      end
      
    rescue => ex
      puts ex.inspect
    end  
    redirect_to admin_product_line_products_path(@product_line)  
  end
  
  
  
  def check_product_download
    begin
      photo_path = "#{Rails.root}/public/upload_download/{*.zip}"

      Dir.glob(photo_path).entries.each do |e|
        file_show = File.basename(e).split('.')[0]
        #file_show = file_show.split('-')[0]
        file_show.gsub!("-", "/")
        puts file_show        
        product =  Product.where(reference: file_show, product_line_id: @product_line).limit(1)
        if !product.blank?
          puts "Produto no Banco: #{product.first.id}"
          params.permit!
          product.first.product_download = File.open(e)
          product.first.save
          File.delete(e)
          puts "Produto Existe"
        else
          puts "Produto não Existe referencia #{file_show}"
        end 
      end
      
    rescue => ex
      puts ex.inspect
    end  
    redirect_to admin_product_line_products_path(@product_line)  
  end
  
  def load_info
    @product_line = ProductLine.find(params[:product_line_id])
    @product_categories = @product_line.product_categories.all_active("priority ASC").collect {|c| [c.title, c.id]} 
    @product_labels = ProductLabel.all_active.collect {|c| [c.title, c.id]} 
  end
  
  
  
end
