class Admin::NoticesController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @notices = Notice.all_active
  end

  def show
    @notice = Notice.find(params[:id])
  end

  def new
    @notice = Notice.new
  end

  def create
    params.permit!
    @notice = Notice.new(params[:notice])
    if @notice.save
      redirect_to [:admin, @notice], :notice => "Successfully created notice."
    else
      render :action => 'new'
    end
  end

  def edit
    @notice = Notice.find(params[:id])
  end

  def update
    params.permit!
    @notice = Notice.find(params[:id])
    if @notice.update_attributes(params[:notice])
      redirect_to [:admin, @notice], :notice  => "Successfully updated notice."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @notice = Notice.find(params[:id])
    @notice.newdestroy
    redirect_to admin_notices_url, :notice => "Successfully destroyed notice."
  end
end
