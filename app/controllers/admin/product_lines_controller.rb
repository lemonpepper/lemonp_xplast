class Admin::ProductLinesController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @product_lines = ProductLine.all
  end

  def show
    @product_line = ProductLine.find(params[:id])
  end

  def new
    @product_line = ProductLine.new
  end

  def create
    @product_line = ProductLine.new(ad_params)
    if @product_line.save
      redirect_to [:admin, @product_line], :notice => "Successfully created product line."
    else
      render :action => 'new'
    end
  end

  def edit
    @product_line = ProductLine.find(params[:id])
  end

  def update
    @product_line = ProductLine.find(params[:id])
    if @product_line.update_attributes(ad_params)
      redirect_to [:admin, @product_line], :notice  => "Successfully updated product line."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @product_line = ProductLine.find(params[:id])
    @product_line.destroy
    redirect_to admin_product_lines_url, :notice => "Successfully destroyed product line."
  end
  
  private

  def ad_params
    params.require(:product_line).permit(*ProductLine.globalize_attribute_names,:slug, :published, :product_line_image)
  end
end
