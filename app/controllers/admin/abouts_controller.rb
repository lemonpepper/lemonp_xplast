class Admin::AboutsController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @abouts = About.all_active
  end

  def show
    @about = About.find(params[:id])
  end

  def new
    @about = About.new
  end

  def create
    params.permit!
    @about = About.new(params[:about])
    if @about.save
      redirect_to [:admin, @about], :notice => "Successfully created about."
    else
      render :action => 'new'
    end
  end

  def edit
    @about = About.find(1)
  end

  def update
    params.permit!
    @about = About.find(1)
    if @about.update_attributes(params[:about])
      redirect_to edit_admin_about_path("xplast"), :notice  => "Successfully updated about."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @about = About.find(params[:id])
    @about.newdestroy
    redirect_to admin_abouts_url, :notice => "Successfully destroyed about."
  end
end
