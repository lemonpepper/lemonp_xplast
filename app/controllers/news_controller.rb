class NewsController < ApplicationController
  layout "blank"
  before_filter :load_configurations
  def index
    @notices = Notice.all_published('created_at desc')
    if !@notices.blank?
      show_currency_exchange(params[:locale])
    else
      root_path
    end    
  end

  def show
    @notice = Notice.find(params[:id])
    @notice_images = @notice.notice_images.all_published
  end
end
