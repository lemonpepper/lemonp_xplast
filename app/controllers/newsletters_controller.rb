class NewslettersController < ApplicationController
  before_filter :load_configurations  
  def create
    params.permit!
    @newsletter = Newsletter.new(params[:newsletter])
    if @newsletter.save
      @erro = false
    else
      @erro = true
    end
  end
  
end
