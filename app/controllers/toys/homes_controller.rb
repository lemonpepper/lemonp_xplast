class Toys::HomesController < ApplicationController
  layout "toys"
  before_filter :load_configurations
  def index
    @product_categories = load_categories(2)
    @products = Product.where(active: true, published: true, product_line_id: 2)
    @type = "highlight-toys"
  end
  
  
  def search
    @product_categories = load_categories(2) 
    if !params[:term].blank?
      @products = Product.search(params[:term])
    end
  end 
end
