class Toys::CategoriesController < ApplicationController
  layout "toys"
  before_filter :load_configurations
  def show
    @product_categories = load_categories(2)
    @product_category = ProductCategory.find(params[:id])
    @products = @product_category.products.where(active: true, published: true).order(:priority)
    @type = "highlight-toys-product"
  end
end
