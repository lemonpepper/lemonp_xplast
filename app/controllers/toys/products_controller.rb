class Toys::ProductsController < ApplicationController
  layout "toys"
  before_filter :load_configurations
  def show
    @product_categories = load_categories(2)
    @product = Product.find(params[:id])
    @product_images = @product.product_images.all_published("priority")
    @type = "highlight-toys-product"
  end
end
