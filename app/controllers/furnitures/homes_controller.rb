class Furnitures::HomesController < ApplicationController
  layout "furniture"
  before_filter :load_configurations
  def index
    @product_categories = load_categories(1)
    @products_news = nil
    @product_featureds = Product.where(active: true, published: true, featured: true, product_line: 1)
  end
  
  def search
    @product_categories = load_categories(1) 
    if !params[:term].blank?
      @products = Product.search(params[:term])
    end
  end
  
end
