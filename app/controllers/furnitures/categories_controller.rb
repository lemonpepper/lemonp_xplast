class Furnitures::CategoriesController < ApplicationController
  layout "furniture"
  before_filter :load_configurations
  def show
    @product_categories = load_categories(1)
    @product_category = ProductCategory.find(params[:id])
    @products = @product_category.products.all_published
  end
end
