class Furnitures::ProductsController < ApplicationController
  layout "furniture"
  before_filter :load_configurations
  def show
    @product_categories = load_categories(1)
    @product = Product.find(params[:id])
    @product_images = @product.product_images.all_published
    @products_news = nil
  end
end
