require 'test_helper'

class Admin::NoticeImagesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => NoticeImage.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    NoticeImage.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    NoticeImage.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_notice_image_url(assigns(:notice_image))
  end

  def test_edit
    get :edit, :id => NoticeImage.first
    assert_template 'edit'
  end

  def test_update_invalid
    NoticeImage.any_instance.stubs(:valid?).returns(false)
    put :update, :id => NoticeImage.first
    assert_template 'edit'
  end

  def test_update_valid
    NoticeImage.any_instance.stubs(:valid?).returns(true)
    put :update, :id => NoticeImage.first
    assert_redirected_to admin_notice_image_url(assigns(:notice_image))
  end

  def test_destroy
    notice_image = NoticeImage.first
    delete :destroy, :id => notice_image
    assert_redirected_to admin_notice_images_url
    assert !NoticeImage.exists?(notice_image.id)
  end
end
