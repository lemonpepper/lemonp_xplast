require 'test_helper'

class Admin::ProductLabelsControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => ProductLabel.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    ProductLabel.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    ProductLabel.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_product_label_url(assigns(:product_label))
  end

  def test_edit
    get :edit, :id => ProductLabel.first
    assert_template 'edit'
  end

  def test_update_invalid
    ProductLabel.any_instance.stubs(:valid?).returns(false)
    put :update, :id => ProductLabel.first
    assert_template 'edit'
  end

  def test_update_valid
    ProductLabel.any_instance.stubs(:valid?).returns(true)
    put :update, :id => ProductLabel.first
    assert_redirected_to admin_product_label_url(assigns(:product_label))
  end

  def test_destroy
    product_label = ProductLabel.first
    delete :destroy, :id => product_label
    assert_redirected_to admin_product_labels_url
    assert !ProductLabel.exists?(product_label.id)
  end
end
