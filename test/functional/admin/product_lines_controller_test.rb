require 'test_helper'

class Admin::ProductLinesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => ProductLine.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    ProductLine.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    ProductLine.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_product_line_url(assigns(:product_line))
  end

  def test_edit
    get :edit, :id => ProductLine.first
    assert_template 'edit'
  end

  def test_update_invalid
    ProductLine.any_instance.stubs(:valid?).returns(false)
    put :update, :id => ProductLine.first
    assert_template 'edit'
  end

  def test_update_valid
    ProductLine.any_instance.stubs(:valid?).returns(true)
    put :update, :id => ProductLine.first
    assert_redirected_to admin_product_line_url(assigns(:product_line))
  end

  def test_destroy
    product_line = ProductLine.first
    delete :destroy, :id => product_line
    assert_redirected_to admin_product_lines_url
    assert !ProductLine.exists?(product_line.id)
  end
end
